# battle-simulator

This project was created according to the following task, used to assess candidates for node js back-end developer position.
<br/>

Task description:
```
https://docs.google.com/document/d/1BXUphDqZnCA0FWwg-uVgbVc-vPG6INEI7AexnvmiJ0I/edit
```

Tiny additions:
- Added API endpoints for stopping the game in process, resuming it, and resetting a finished or stopped game (to ease testing)

Postman requests location:
```
src/postman/battle-simulator.postman_collection.json
```

Environment:
- OS: 
    ```
    MacOS High Sierra, Version: 10.13.6
    ```
- IDE:
    ```
    WebStorm, 2019.3
    ```

Database configuration:
- Used Mongodb installed locally, instructed by the official docs:

    ```
    https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/
    ```
- Used default path for mongodb loaded from .env (more info at mongodb connection module itself: src/database/mongodb.js):
    ```
    MONGODB_HOST=localhost
    MONGODB_PORT=27017
    ```
<br/>

To install dependencies:
```
yarn install
```

To start the app type:
```
yarn start
```

Task Author:
```
Zoran Lazic
https://www.linkedin.com/in/puhna/
```

Application Developer:
```
Stefan Djordjevic
https://www.linkedin.com/in/stefan-djordjevic-51a852183/
```

All best.
