const express = require('express');
const GameModel = require('../models/game');
const LogModel = require('../models/log');

// Exceptions
const BadRequestError = require('../exceptions/badrequest');
const NotFoundError = require('../exceptions/notfound');
const UnprocessableEntity = require('../exceptions/unprocessableEntity');

// Constants
const {
	GAME_STATUSES,
	ATTACK_STRATEGIES,
} = require('../lib/constants');

// Battle Generator Class
const Battle = require('../lib/battle');

// Global Battle instance
const MyBattle = require('../lib/battleInstanceGlobal');

const GameRouter = express.Router();

// Create a game and retrieve its id
GameRouter.post('/create-game', async (req, res, next) => {
	const {
		name,
	} = req.body;

	try {
		const game = await GameModel.create({
			name,
			status: 'not started',
			armies: [],
		});

		const { _id: gameId } = game;

		LogModel.create({ gameId });

		res.locals.data = { gameId };

		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

// Adding army to game that's not started
GameRouter.put('/:gameId/add-army', async (req, res, next) => {
	const {	gameId } = req.params;
	const army = req.body;

	try {
		const {
			name,
			units,
			attackStrategy,
		} = army;

		if (typeof name !== 'string' || !name.length) {
			throw new UnprocessableEntity('Unprocessable Entity', 'Army name of must be a string at least 1 character long.');
		}

		const strategiesArray = Object.values(ATTACK_STRATEGIES);

		if (!strategiesArray.find(strategy => strategy === attackStrategy)) {
			throw new UnprocessableEntity('Unprocessable Entity', `Army strategy must one of the following\n${strategiesArray}.`);
		}

		if (typeof units !== 'number' || units < 80 || units > 100) {
			throw new UnprocessableEntity('Unprocessable Entity', 'Army units must be a number between 80 and 100 (both inclusive).');
		}
		const game = await GameModel.findById(gameId);

		if (!game) {
			throw new NotFoundError('Game Not Found', 'No game was found by the given Id.');
		}

		const {
			status,
			armies,
		} = game;

		if (status !== GAME_STATUSES.NOT_STARTED) {
			throw new BadRequestError('Bad Request', 'Units cannot be added, the game has already started.');
		}

		armies.push(army);

		await game.updateOne({ armies });

		res.locals.data = game;
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

// Get a list of all games
GameRouter.get('/all-games', async (req, res, next) => {
	try {
		res.locals.data = await GameModel.find({});
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

// Puts a not started game in process
GameRouter.put('/:gameId/start-game', async (req, res, next) => {
	const {	gameId } = req.params;

	try {
		const game = await GameModel.findById(gameId);

		if (!game) {
			throw new NotFoundError('Game Not Found', 'No game was found by the given Id.');
		}

		const {
			status,
			armies,
		} = game;

		if (status === GAME_STATUSES.IN_PROCESS) {
			throw new BadRequestError('Bad Request', 'Cannot start the game, it is already in process.');
		}

		if (status === GAME_STATUSES.FINISHED) {
			throw new BadRequestError('Bad Request', 'This game is finished. Please reset if you want to start again.');
		}

		if (status === GAME_STATUSES.STOPPED) {
			throw new BadRequestError('Bad Request', 'This game is stopped. Please resume it if you want to play again.');
		}

		const armiesMinimum = process.env.ARMIES_MINIMUM || 5;
		if (armies.length < armiesMinimum) {
			throw new BadRequestError('Bad Request', `Minimum ${armiesMinimum} players are needed to start the game.`);
		}

		game.status = GAME_STATUSES.IN_PROCESS;
		game.save();

		MyBattle.instance = new Battle(armies, gameId);
		await MyBattle.instance.startBattle();

		res.locals.data = game;

		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

// Stops a game in process
GameRouter.put('/:gameId/stop-game', async (req, res, next) => {
	const {	gameId } = req.params;

	try {
		const game = await GameModel.findById(gameId);

		if (!game) {
			throw new NotFoundError('Game Not Found', 'No game was found by the given Id.');
		}

		if (game.status !== GAME_STATUSES.IN_PROCESS) {
			throw new BadRequestError('Bad Request', 'The game cannot be stopped, it is not in process.');
		}

		if (!MyBattle.instance || MyBattle.instance.gameId.toString() !== gameId) {
			throw new NotFoundError('Game Not Found', 'No game was found in process by the given Id.');
		}

		MyBattle.instance.stopGame();

		game.status = GAME_STATUSES.STOPPED;
		await game.save();

		res.locals.data = game;
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

// Resumes game that was stopped by the user
GameRouter.put('/:gameId/resume-game', async (req, res, next) => {
	const {	gameId } = req.params;

	try {
		const game = await GameModel.findById(gameId);

		if (!game) {
			throw new NotFoundError('Game Not Found', 'No game was found by the given Id.');
		}

		if (game.status !== GAME_STATUSES.STOPPED) {
			throw new BadRequestError('Bad Request', 'The game cannot be resumed, it is not stopped.');
		}

		// Different process - user stopped the game and then process got killed, now user resumes it
		if (!MyBattle.instance || MyBattle.instance.gameId !== gameId) {
			const {
				_id: gameId,
				armies,
			} = game;

			MyBattle.instance = new Battle(armies, gameId.toString());
			await MyBattle.instance.startBattle();

			game.status = GAME_STATUSES.IN_PROCESS;
			await game.save();

			res.locals.data = game;
			return next(null);
		}

		// Same process - user stopped it and now resumes it
		if (MyBattle.instance && MyBattle.instance.gameId === gameId) {
			MyBattle.instance.resumeGame();

			game.status = GAME_STATUSES.IN_PROCESS;
			await game.save();

			res.locals.data = game;
			return next(null);
		}

		res.locals.data = game;
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

// Resets finished game (deletes game logs and sets game status to not_started - used to ease the testing, can ignore this)
GameRouter.put('/:gameId/reset-game', async (req, res, next) => {
	const {	gameId } = req.params;

	try {
		const game = await GameModel.findById(gameId);

		if (!game) {
			throw new NotFoundError('Game Not Found', 'No game was found by the given Id.');
		}

		if (game.status === GAME_STATUSES.IN_PROCESS) {
			throw new BadRequestError('Game is in process', 'Please wait for game to end if you want to start it again.');
		}

		const log = await LogModel.findOne({ gameId });

		log.logs = [];
		log.save();

		game.status = GAME_STATUSES.NOT_STARTED;
		game.save();

		MyBattle.instance = null;

		res.locals.data = game;
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

module.exports = GameRouter;
