const express = require('express');

const router = express.Router();

router.use('/api', require('./game'));
router.use('/api', require('./log'));

module.exports = router;
