const express = require('express');
const LogModel = require('../models/log');

// Exceptions
const BadRequestError = require('../exceptions/badrequest');
const NotFoundError = require('../exceptions/notfound');

const LogRouter = express.Router();

LogRouter.get('/:gameId/game-log', async (req, res, next) => {
	const {	gameId } = req.params;
	try {
		const log = await LogModel.findOne({ gameId });

		if (!log) {
			throw new NotFoundError('Log Not Found', 'No log was find by the given Id.');
		}

		res.locals.data = log;
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

LogRouter.get('/:gameId/game-log-sorted', async (req, res, next) => {
	const {	gameId } = req.params;
	try {
		const log = await LogModel.findOne({ gameId });

		if (!log) {
			throw new NotFoundError('Log Not Found', 'No log was find by the given Id.');
		}

		log.logs.sort((a, b) => a.timestamp - b.timestamp);
		res.locals.data = log;
		return next(null);
	} catch (error) {
		console.log(error);
		return next(error);
	}
});

module.exports = LogRouter;
