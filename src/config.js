const dotenv = require('dotenv');

const config = {};
if (process.env.ENV_FILE) {
	config.path = `./${process.env.ENV_FILE}`;
	console.log(`Loading env configuration file ${process.env.ENV_FILE}`);
} else if (process.env.NODE_ENV) {
	config.path = `./.env.${process.env.NODE_ENV}`;
	console.log(`Loading env configuration for ${process.env.NODE_ENV}`);
} else {
	console.log('Loading default env configuration');
}

const result = dotenv.config(config);

if (result.error) {
	console.warn(result.error); // could be normal in some cloud environments where configuration comes from actual env vars and not from a file
} else {
	console.log(`Loaded ${Object.keys(result.parsed).length} configuration properties.`);
}

module.exports = dotenv;
