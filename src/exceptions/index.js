class BaseError extends Error {
	constructor(message, description) {
		super(message);

		this.message = message;
		this.description = description;
	}

	toJSON() {
		return {
			code: this.code,
			message: this.message,
			description: this.description,
		};
	}
}

module.exports = BaseError;
