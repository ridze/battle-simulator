require('./config'); // load dotenv
const express = require('express');
const bodyParser = require('body-parser');

// Connect to database
require('./database/mongodb');

// First check for unfinished game and start it again
require('./lib/checkForUnfinishedGame');

const formatterMiddleware = require('./middleware/formatter');
const errorMiddleware = require('./middleware/error');

const NotFoundError = require('./exceptions/notfound');

const app = express();
const router = express.Router();

app.use(router);
app.use(bodyParser.json());

// routes
app.use(require('./api/index'));

app.use(async (req, res, next) => {
	if (!res.locals.data) {
		return next(new NotFoundError('Route not matched'));
	}

	return next(null);
});

app.use(formatterMiddleware);
app.use(errorMiddleware);

app.listen(3000, () => {
	console.log('Server listening on port: 3000');
});
