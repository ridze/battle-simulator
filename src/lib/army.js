// CONSTANTS
const { ATTACK_STRATEGIES } = require('../lib/constants');

class Army {
	constructor(name, units, attackStrategy, armyId, saveAttackToLogCallback) {
		this.name = name;
		this.units = units;
		this.attackStrategy = attackStrategy;
		this.armyId = armyId;
		this.saveAttackToLogCallback = saveAttackToLogCallback;
	}

	// In Milliseconds
	getReloadTime() {
		const { units } = this;
		return 10 * units;
	}

	getTarget(aliveOpponentArmies) {
		const { attackStrategy } = this;

		switch (attackStrategy) {
			case ATTACK_STRATEGIES.WEAKEST: {
				let weakestTarget = aliveOpponentArmies[0];
				aliveOpponentArmies.forEach((army) => {
					if (weakestTarget.units > army.units) {
						weakestTarget = army;
					}
				});
				return weakestTarget;
			}
			case ATTACK_STRATEGIES.STRONGEST: {
				let strongestTarget = aliveOpponentArmies[0];
				aliveOpponentArmies.forEach((army) => {
					if (strongestTarget.units < army.units) {
						strongestTarget = army;
					}
				});
				return strongestTarget;
			}
			case ATTACK_STRATEGIES.RANDOM: {
				const { length: aliveOpponentArmiesNumber } = aliveOpponentArmies;
				const randomArmyIndex = Math.floor(Math.random() * aliveOpponentArmiesNumber);
				return aliveOpponentArmies[randomArmyIndex];
			}
		}
	}

	getDamageToDeal() {
		const { units } = this;
		if (units === 1) {
			return 1;
		}
		return units * 0.5;
	}

	getAttackChances() {
		const { units } = this;
		return units;
	}

	isAlive() {
		const { units } = this;
		return units > 0;
	}

	attack(aliveOpponentArmies) {
		const attackChances = this.getAttackChances();
		const isAttackSuccessful = attackChances > (Math.random() * 100 + 1);
		const attackDamage = isAttackSuccessful ? this.getDamageToDeal() : 0;
		const target = this.getTarget(aliveOpponentArmies);

		if (!target) {
			return;
		}

		target.units -= attackDamage;
		const timestamp = new Date().getTime();

		const {
			armyId: attackerArmyId,
			name,
			units,
			attackStrategy,
		} = this;

		const {
			armyId: targetArmyId,
			name: targetName,
			units: targetUnits,
		} = target;

		console.log(`${name}, units: ${units} ---> ${targetName}, units: ${targetUnits + attackDamage}`);
		console.log(`strategy: ${attackStrategy}`);
		console.log(`damage: ${attackDamage}`);
		console.log(`target units left: ${targetUnits}\n`);

		const data = {
			attackerArmy: {
				attackerArmyId,
				name,
				units,
				attackStrategy,
			},
			targetArmy: {
				targetArmyId,
				name: targetName,
				units: targetUnits + attackDamage,
				unitsLeft: targetUnits,
			},
			timestamp,
			damage: attackDamage,
		};

		this.saveAttackToLogCallback(data);
	}
}

module.exports = Army;
