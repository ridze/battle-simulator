const EventEmitter = require('events');

const GameModel = require('../models/game');
const LogModel = require('../models/log');

const Army = require('./army');

const {
	GAME_STATUSES,
	GAME_EVENTS,
} = require('../lib/constants');

class Battle extends EventEmitter {
	constructor(armies, gameId) {
		super();
		this.gameStopped = false;
		this.gameId = gameId;
		this.armies = armies.map((army) => {
			const {
				_id: armyId,
				name,
				units,
				attackStrategy,
			} = army;
			return new Army(name, units, attackStrategy, armyId.toString(), data => this.saveAttackToLog(data, gameId));
		});

		// All armies engage in battle on START_BATTLE event
		this.armies.forEach((army) => this.on(GAME_EVENTS.START_BATTLE, () => this.putArmyIntoBattle(army)));

		// Callback to update status on game over
		this.on(GAME_EVENTS.GAME_OVER, async () => {
			try {
				const game = await GameModel.findById(gameId);
				game.status = GAME_STATUSES.FINISHED;
				await game.save();
			} catch (error) {
				console.log(error);
			}
		});
	}

	async saveAttackToLog(data, gameId) {
		const logInstance = await LogModel.findOne({ gameId });
		logInstance.logs.push(data);
		logInstance.save();
	}

	// Checks if it should update armies units - if log is not empty
	async startBattle() {
		await this.updateArmiesUnits();
		this.emit(GAME_EVENTS.START_BATTLE);
	}

	// Used when user stopped the game through the API - just remove flag and emit START_BATTLE event again
	resumeGame() {
		this.gameStopped = false;
		this.emit(GAME_EVENTS.START_BATTLE);
		console.log('GAME RESUMED.');
		console.log('\n');
	}

	// Used when user stops battle though API - just setting gameStopped flag to true
	stopGame() {
		this.gameStopped = true;
		console.log('GAME STOPPED.');
		console.log('\n');
	}

	gameOver() {
		this.emit(GAME_EVENTS.GAME_OVER);
	}

	// Used when process was killed - updates armies before resuming the battle
	async updateArmiesUnits() {
		const {
			gameId,
			armies,
		} = this;

		const gameLog = await LogModel.findOne({ gameId });

		const { logs } = gameLog;

		// No logs - no update
		if (!logs.length) {
			return;
		}

		const updatedArmies = {};

		// Reverse logs to find latest occurrence of armies being attacked (target), and use unitsLeft to update their current units number
		const reversedLogs = logs.reverse();

		let i = 0;
		while (Object.keys(updatedArmies).length < armies.length && i < reversedLogs.length) {
			const currentLog = reversedLogs[i];
			const { targetArmy } = currentLog;

			const {
				targetArmyId,
				unitsLeft,
			} = targetArmy;

			// If armies units are not updated
			if (!updatedArmies[targetArmyId]) {
				updatedArmies[targetArmyId] = true;
				const lastArmyOccurredAsTarget = armies.find(army => (army.armyId === targetArmyId));
				lastArmyOccurredAsTarget.units = unitsLeft;
			}
			i += 1;
		}

		console.log('UPDATED ARMIES (ready to finish the fight!):');
		console.log(armies);
	}

	putArmyIntoBattle(currentArmy) {
		setTimeout(() => {
			const aliveOpponentArmies = this.armies.filter(army => army.units > 0 && army !== currentArmy);

			if (this.gameStopped) {
				return;
			}

			if (currentArmy.isAlive() && aliveOpponentArmies.length > 0) {
				currentArmy.attack(aliveOpponentArmies);
				this.putArmyIntoBattle(currentArmy);
			} else if (!aliveOpponentArmies.length && currentArmy.isAlive()) {
				console.log(`THE WINNER IS: ${currentArmy.name}`);
				this.gameOver();
			}
		}, currentArmy.getReloadTime());
	}
}

module.exports = Battle;
