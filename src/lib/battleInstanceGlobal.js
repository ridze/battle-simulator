// This module (just an object) with an instance property
// that is used to keep an instance of a Battle class
// and make it usable throughout the other app modules

module.exports = {
	instance: null,
};
