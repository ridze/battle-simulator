const GameModel = require('../models/game');

// Constants
const { GAME_STATUSES } = require('../lib/constants');

// Battle Generator Class
const Battle = require('../lib/battle');

// Global Battle instance
const MyBattle = require('../lib/battleInstanceGlobal');

module.exports = (async () => {
	try {
		console.log('Checking for game in process...');

		const gameInProcess = await GameModel.findOne({ status: GAME_STATUSES.IN_PROCESS });

		if (gameInProcess) {
			console.log('GAME IN PROCESS:');
			console.log(gameInProcess);

			const {
				armies,
				_id: gameId,
			} = gameInProcess;

			console.log('Updating armies to finish the battle...');

			MyBattle.instance = new Battle(armies, gameId.toString());
			await MyBattle.instance.startBattle();
		} else {
			console.log('No games to finish...');
		}
	} catch (error) {
		console.log('There was an error while recreating game.');
		console.log(error);
	}
}) ();
