const GAME_STATUSES = {
	NOT_STARTED: 'not started',
	IN_PROCESS: 'in process',
	STOPPED: 'stopped',
	FINISHED: 'finished',
};

const ATTACK_STRATEGIES = {
	WEAKEST: 'Weakest',
	STRONGEST: 'Strongest',
	RANDOM: 'Random',
};

const GAME_EVENTS = {
	START_BATTLE: 'start_battle',
	GAME_OVER: 'game_over',
};

module.exports = {
	GAME_STATUSES,
	ATTACK_STRATEGIES,
	GAME_EVENTS,
};
