const InternalServerError = require('../exceptions/internalserver');
const BaseError = require('../exceptions/');

module.exports = function error(err, req, res, next) { // eslint-disable-line no-unused-vars
	if (err instanceof Error) {
		if (!err.code) { // if error has no code property, it might mean that this is a system exception and not one of our own exceptions
			return next(err);
		}

		// validate http code in the error
		let { code } = err;

		code = Number.isInteger(code) && code > 0 && code < 600 ? code : 500;

		Object.assign(err, {
			toJSON: BaseError.prototype.toJSON,
			code,
		});

		// construct json object to return in response
		const response = {
			error: err.toJSON(),
		};

		return res.status(code).json(response);
	}

	return res.status(500).json(new InternalServerError(err));
};
