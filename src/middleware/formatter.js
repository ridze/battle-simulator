const NotImplementedError = require('../exceptions/notimplemented');

function formatter(req, res, next) {
	res.format({
		default() {
			return next(new NotImplementedError());
		},

		json() {
			const data = {};

			data.data = res.locals.data;

			return res.json(data);
		},
	});
}

module.exports = formatter;
