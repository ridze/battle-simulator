const mongoose = require('../database/mongodb');

const {
	Schema,
	model,
} = mongoose;

const gameSchema = new Schema({
	name: String,
	status: String,
	armies: [{
		name: String,
		units: Number,
		attackStrategy: String,
	}],
});

const Game = model('Game', gameSchema);

module.exports = Game;
