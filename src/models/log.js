const mongoose = require('../database/mongodb');

const {
	Schema,
	model,
} = mongoose;

const logSchema = new Schema({
	gameId: String,
	logs: [{
		attackerArmy: {
			attackerArmyId: String,
			name: String,
			units: Number,
			attackStrategy: String,
		},
		targetArmy: {
			targetArmyId: String,
			name: String,
			units: Number,
			unitsLeft: Number,
		},
		damage: Number,
		timestamp: Number, // Milliseconds
	}],
});

const Log = model('Log', logSchema);

module.exports = Log;
